# I should have emacs
set -gx EDITOR emacs
set -gx TERM xterm-24bit
set -gx JUPYTERLAB_DIR $HOME/.local/share/jupyter/lab

# Set terminfo
if not test -f ~/.terminfo/x/xterm-24bit
    echo "
xterm-24bit|xterm with 24-bit direct color mode,
   use=xterm-256color,
   sitm=\E[3m,
   ritm=\E[23m,
   setb24=\E[48;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,
   setf24=\E[38;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,
" > /tmp/xterm-24bit.terminfo
    tic -x -o ~/.terminfo /tmp/xterm-24bit.terminfo
end

if test -d /opt/cuda/bin/
    set -x PATH /opt/cuda/bin $PATH
end


# Setup starshup
if which starship &>/dev/null
    starship init fish | source
end

# Ruby gem
if which ruby &>/dev/null
    set -gx GEM_HOME (ruby -e 'print Gem.user_dir')
    fish_add_path -p $GEM_HOME/bin
end

for bin_dir in $HOME/.local/bin  \
               $HOME/.cargo/bin  \
               $HOME/Android/Sdk/platform-tools \
               $HOME/.npm-global/bin \
               $HOME/.node-packages/bin
    if test -d $bin_dir
        fish_add_path -p $bin_dir
    end
end

# Keychain config
if which keychain &>/dev/null
    if status --is-interactive
        for sshkey in id_ed25519 id_ed25519_km
            if test -f $HOME/.ssh/$sshkey
                keychain --eval $sshkey --quiet --nogui &>/dev/null
            end
        end
    end

    begin
        set -l HOSTNAME (hostname)
        if test -f ~/.keychain/$HOSTNAME-fish
            source ~/.keychain/$HOSTNAME-fish
        end
    end
end
